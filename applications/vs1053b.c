
/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2019-04-13     XiaojieFan   the first version
 * 2019-12-04     RenMing      ADD PAGE WRITE and input address can be selected 
 */
/*
硬件连接：
cmbx:
    PC10     ------> SPI3_SCK
    PC11     ------> SPI3_MISO
    PC12     ------> SPI3_MOSI

    mcu                 VS1053B
    PC10----------------SPI3SCK

    (SPI3_MISO)PC11----------------SPI3MISO
    (SPI3_MOSI)PC12----------------SPI3MOSI

    PA15----------------SPI3CS     XCS 片选输入（低有效） 
 
    PD2--------------------------->XDCS 数据片选/字节同步
    PD4<---------------------------DREQ 数据请求  DREQ 是数据请求线，用于通知控制器， VS1053B是否可以接收数据。 配置为中断IO
    PD6--------------------------->RST 复位引脚（硬复位，低电平有效）
    5V                             VCC  
                                   GND
*/
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include <rtdbg.h>
#include <dfs_posix.h>
#include "drv_spi.h"
#include "vs1053b.h"


static struct rt_spi_device    *spi_dev_VS1053B;  /* SPI设备VS1053B对象 */
static rt_sem_t dreq_sem       = RT_NULL;

static void DREQ_irq_callback(void *args)
{
    rt_uint32_t sign = (rt_uint32_t)args;
    switch (sign)
    {
			case DREQ_PIN:
           rt_sem_release(dreq_sem);
				   break;
			default:
					 LOG_E("error sign= %d !\n", sign);
					 break;
    }
}

static int vs1053b_ExPinConfig(void)
{	 
    rt_pin_mode(XDCS_PIN,  PIN_MODE_OUTPUT);
    rt_pin_mode(RST_PIN,   PIN_MODE_OUTPUT);

		rt_pin_mode(DREQ_PIN, PIN_MODE_INPUT_PULLUP);
		rt_pin_attach_irq(DREQ_PIN, PIN_IRQ_MODE_RISING, DREQ_irq_callback, (void *)DREQ_PIN);//上升源 触发中断回调函数
		rt_pin_irq_enable(DREQ_PIN, PIN_IRQ_ENABLE);

    return 1;
}	

//等待空闲
static rt_int8_t WaitDreqHigh(rt_int16_t ms)
{
  if(rt_pin_read(DREQ_PIN)==1) 
  return RT_EOK;

  dreq_sem->value=0;	
  if(RT_EOK==rt_sem_take(dreq_sem, ms))
  {
     return RT_EOK;
  }
  else 
     return RT_ERROR;
}

//CS必须为释放状态
static void WriteBytesToVs1053b(rt_uint8_t *buffer,rt_uint16_t Len)
{
		struct rt_spi_message msg1;
		msg1.send_buf   = buffer;
		msg1.recv_buf   = RT_NULL;
		msg1.length     = Len;
		msg1.cs_take    = 0;  //msg1.cs_take    = 1; × 
		msg1.cs_release = 0;
		msg1.next       = RT_NULL;
		rt_spi_transfer_message(spi_dev_VS1053B, &msg1);
}

//读寄存器 
rt_uint16_t ReadRegisterByVS1053B(rt_uint8_t address)
{
	    uint8_t send_buff[2] = {0};
      uint8_t read_buff[2] = {0};
      rt_uint16_t temp=0;   
			send_buff[0] = VS_READ_COMMAND;
			send_buff[1] = address;
      WaitDreqHigh(RT_WAITING_FOREVER);
      XDCS_PIN_H; 
			rt_spi_send_then_recv(spi_dev_VS1053B,send_buff,2, read_buff,2);
      temp=read_buff[0];
      temp=temp<<8;
      temp+=read_buff[1];
      return temp;
}

//写寄存器
void WriteRegisterByVS1053B(rt_uint8_t address,rt_uint16_t data)
{
  uint8_t send_buff[2];
  rt_uint16_t writedata=0;
	send_buff[0] = VS_WRITE_COMMAND;
	send_buff[1] = address;
  //高低8位交换
  writedata = ( (data&0x00ff) <<8);
  writedata+= ( (data&0xff00) >>8);
  WaitDreqHigh(RT_WAITING_FOREVER);
  XDCS_PIN_H;
	rt_spi_send_then_send(spi_dev_VS1053B, send_buff, 2, &writedata, 2);
}

rt_int8_t VS1053B_HardwareReset(void)
{
  RST_PIN_L;
	rt_thread_mdelay(20);
  RST_PIN_H;	

  WaitDreqHigh(RT_WAITING_FOREVER);

  if(ReadRegisterByVS1053B(SPI_MODE)!=0x4800)
  {
     LOG_E("DevHdReset is ERR\r\n");
     return RT_ERROR;
  }
  return RT_EOK;
}

rt_int8_t VS1053B_SoftReset(void)
{
  WaitDreqHigh(RT_WAITING_FOREVER);
  rt_uint16_t RegData=ReadRegisterByVS1053B(SPI_MODE);
  RegData |=  (1<<2);  
  WaitDreqHigh(RT_WAITING_FOREVER);
  WriteRegisterByVS1053B(SPI_MODE,RegData);
}

//取消解码
rt_int8_t VS1053B_Cancel(void)
{
  WaitDreqHigh(RT_WAITING_FOREVER);
  rt_uint16_t RegData=ReadRegisterByVS1053B(SPI_MODE);
  RegData |=  (1<<3);  
  WaitDreqHigh(RT_WAITING_FOREVER);
  WriteRegisterByVS1053B(SPI_MODE,RegData);
}

//volx:音量大小(0~254)
void vs1053b_VolumeSet(rt_uint16_t volx)
{
    rt_uint16_t volt=0; 			  //暂存音量值
    volt=254-volx;			        //取反一下,得到最大值,表示最大的表示 
	  volt<<=8;
    volt+=254-volx;			        //得到音量设置后大小
    WriteRegisterByVS1053B(SPI_VOL,volt); //设音量 
}
//音量测试通过， 硬件复位会恢复最大声音
static int volset(int argc, char** argv)
{
   vs1053b_VolumeSet(strtoul(argv[1], 0, 10));
}
MSH_CMD_EXPORT(volset, volset);

//设定高低音控制
//bfreq:低频上限频率	2~15(单位:10Hz)
//bass:低频增益			  0~15(单位:1dB)
//tfreq:高频下限频率 	1~15(单位:Khz)
//treble:高频增益  	 	0~15(单位:1.5dB,小于9的时候为负数)
void vs1053b_SetBass(rt_uint8_t bfreq,rt_uint8_t bass,rt_uint8_t tfreq,rt_uint8_t treble)
{
  rt_uint16_t bass_set=0; //暂存音调寄存器值
  signed char temp=0;   	 
	if(treble==0)temp=0;	   		//变换
	else if(treble>8)temp=treble-8;
 	else temp=treble-9;  
	bass_set=temp&0X0F;				//高音设定
	bass_set<<=4;
	bass_set+=tfreq&0xf;			//高音下限频率
	bass_set<<=4;
	bass_set+=bass&0xf;				//低音设定
	bass_set<<=4;
	bass_set+=bfreq&0xf;			//低音上限    
	WriteRegisterByVS1053B(SPI_BASS,bass_set);	//BASS 
}

//设定音效
//eft:0,关闭;1,最小;2,中等;3,最大.
void vs1053b_SetEffect(rt_uint8_t eft)
{
	rt_uint16_t temp;	 
	temp=ReadRegisterByVS1053B(SPI_MODE);	//读取SPI_MODE的内容
	if(eft&0X01)temp|=1<<4;		//设定LO
	else temp&=~(1<<5);			//取消LO
	if(eft&0X02)temp|=1<<7;		//设定HO
	else temp&=~(1<<7);			//取消HO						   
	WriteRegisterByVS1053B(SPI_MODE,temp);	//设定模式    
}

//控制寄存器读写测试
int8_t RegChangeTest(void)
{
  //读取SPI_MODE的内容
  rt_uint16_t RegData=ReadRegisterByVS1053B(SPI_MODE);
  LOG_W("SPI_MODE=0x%4x\r\n", RegData); 
  WaitDreqHigh(RT_WAITING_FOREVER);
  RegData |=  (1<<13);  
  WaitDreqHigh(RT_WAITING_FOREVER);
  WriteRegisterByVS1053B(SPI_MODE,RegData);
  LOG_W("SPI_MODE=0x%4x\r\n",  ReadRegisterByVS1053B(SPI_MODE));
}
MSH_CMD_EXPORT(RegChangeTest, check read wrtie reg fuction );



#ifdef DBG_VS1053B_ENABLE
//正弦测试：可以听到  嘟---声音 pass
const char SineTest1[]={0x53,0xef,0x6e,0x24,0x00,0x00,0x00,0x00};
const char SineTest2[]={0x45,0x78,0x69,0x74,0x00,0x00,0x00,0x00};	
const char SineTest3[]={0x53,0xef,0x6e,0x44,0x00,0x00,0x00,0x00};		
void VS1053B_Sine_Test(void)
{					
   VS1053B_HardwareReset();
   WriteRegisterByVS1053B(0x0b,0X2020);//设置音量	
   WriteRegisterByVS1053B(SPI_MODE,0x0820);
   WaitDreqHigh(RT_WAITING_FOREVER);
   XDCS_PIN_L;
   WriteBytesToVs1053b((rt_uint8_t*)SineTest1,sizeof SineTest1);
   XDCS_PIN_H;
   XDCS_PIN_L;
   WriteBytesToVs1053b((rt_uint8_t*)SineTest2,sizeof SineTest2);
   XDCS_PIN_H;

   XDCS_PIN_L;
    WriteBytesToVs1053b((rt_uint8_t*)SineTest3,sizeof SineTest3);
   XDCS_PIN_H;
	    
   XDCS_PIN_L;
   WriteBytesToVs1053b((rt_uint8_t*)SineTest1,sizeof SineTest1);
   XDCS_PIN_H;

   LOG_W("VS1053B_Sine_Test is doing\r\n"); 
}	 
MSH_CMD_EXPORT(VS1053B_Sine_Test, VS1053B_Sine_Test );
#endif

//RAM测试
const char ramTest[]={0x4d,0xea,0x6d,0x54,0x00,0x00,0x00,0x00};																			 
u16 VS1053B_Ram_Test(void)
{ 
   VS1053B_HardwareReset();
   WriteRegisterByVS1053B(SPI_MODE,0x0820);

   WaitDreqHigh(RT_WAITING_FOREVER);
   XDCS_PIN_L;
   WriteBytesToVs1053b((rt_uint8_t*)ramTest,sizeof ramTest);
   XDCS_PIN_H;
   rt_thread_mdelay(150);  

   if(0x83ff==ReadRegisterByVS1053B(SPI_HDAT0))// VS1053B如果得到的值为0x83FF，则表明完好
   {
      LOG_W("VS1053B_Ram_Test is passed\r\n"); 
   }
   else LOG_E("VS1053B_Ram_Test is failed\r\n");       
}
MSH_CMD_EXPORT(VS1053B_Ram_Test, VS1053B_Ram_Test );


//测试功能
void vs1053b(int argc, char *argv[])
{
    int i;
    for (i=0; i < argc; i++)
    printf("Argument %d is %s.\n", i, argv[i]);

    if (argc > 1)
    {
        if (!strcmp(argv[1], "play"))
        {
             

        }
        else if(!strcmp(argv[1], "test"))
        {

        }
    }
    else
    {
       rt_kprintf("Usage: do it again\n");
    }
}
MSH_CMD_EXPORT(vs1053b, vs1053b function);


//音乐播放线程
//WAV   支持44.1K  双声道  测试通过
//mp3   支持24K    双声道  测试通过
static void music_play_thread(void *parameter)
{
   rt_uint16_t dataLen=32;
   rt_uint8_t *databuff=rt_malloc(dataLen);
   if(databuff==RT_NULL)
   {
       LOG_E("ERR:databuff");
       return;
   }
   rt_uint16_t num=0;
  
  // VS1053B_HardwareReset();//硬件复位恢复默认参数
  // vs1053b_VolumeSet(180); //音量控制,可以控制支持
   VS1053B_SoftReset();
  
   int fd =RT_NULL;

 //fd= open("/yxr.mp3", O_RDONLY); 
   fd= open("/CC44K.wav", O_RDONLY); 
   if(fd>0)
   {
       rt_memset(databuff,0,dataLen);
       if(read(fd, databuff,dataLen)==dataLen)
       {
           WaitDreqHigh(2000);
           XDCS_PIN_L;
           while(1)
           {
              WaitDreqHigh(2000);
              WriteBytesToVs1053b((rt_uint8_t*)databuff,dataLen);
              if(read(fd, databuff,dataLen)!=dataLen) break;
           }
       }
       XDCS_PIN_H;
       close(fd);     
   }
   else
   {
      LOG_E("ERR:open music file\r\n");
   }
   rt_free(databuff);
   LOG_W("PlayMusicTest end\r\n");
}

static void music_play_test(void)
{
	rt_thread_t tid = rt_thread_create("musicplay",music_play_thread, RT_NULL,1024,15, 5);
  if (tid != RT_NULL)rt_thread_startup(tid);
}
MSH_CMD_EXPORT(music_play_test, create a music_play_thread );


//注册SPI设备
static int rt_hw_VS1053B_register()
{
		rt_err_t res = RT_EOK;
    res = rt_hw_spi_device_attach(SPI_BUS_NAME, SPI_VS1053B_DEVICE_NAME, CS_PIN_GROUP, CS_PIN_NUM);
	  if (res != RT_EOK)
		{
			LOG_E("vs1053b Failed to attach device %s\n", SPI_VS1053B_DEVICE_NAME);
			return res;
		}
    return RT_EOK;
}
INIT_COMPONENT_EXPORT(rt_hw_VS1053B_register);


static int vs1053b_DevInit(void)
{
    spi_dev_VS1053B = (struct rt_spi_device *)rt_device_find(SPI_VS1053B_DEVICE_NAME);
    if (!spi_dev_VS1053B)
    {
        LOG_E("spi find run failed! cant't find %s device!\n", SPI_VS1053B_DEVICE_NAME);
        return RT_ERROR;
    }
 		struct rt_spi_configuration cfg = {0};
		cfg.data_width = 8;
		cfg.mode = RT_SPI_MASTER |RT_SPI_MODE_0 | RT_SPI_MSB ;//| RT_SPI_NO_CS;
    cfg.max_hz = 10 * 1000 *1000; /* 20M,SPI max */
		rt_spi_configure(spi_dev_VS1053B, &cfg);

    vs1053b_ExPinConfig();

    dreq_sem = rt_sem_create("drsem", 0, RT_IPC_FLAG_PRIO);
    if (dreq_sem == RT_NULL)
    {
        rt_kprintf("create dreq_sem semaphore failed.\n");
        return RT_ERROR;
    }
    else
    {
        rt_kprintf("create done. dreq_sem semaphore value = 0.\n");
    }
 
    VS1053B_HardwareReset();
 
		return RT_EOK;
}
INIT_APP_EXPORT(vs1053b_DevInit);



