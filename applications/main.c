/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-06     SummerGift   first version
 * 2018-11-19     flybreak     add stm32f407-atk-explorer bsp
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include <dfs_posix.h> /* 当需要使用文件操作时，需要包含这个头文件 */
#include <rtdbg.h>
#include "ftp.h"

/* defined the LED0 pin: PF9 */
#define LED0_PIN    GET_PIN(F, 9)

void InitFileSys(void)
{
    if(dfs_mount("W25Q128","/","elm",0,0)==0) //系统挂载
    {
        LOG_W("InitFlieSys is right\r");
    }
    else
    {
        LOG_E("ERR:InitFlieSys\r");
    }
}

int main(void)
{
    int count = 1;
    /* set LED0 pin mode to output */
    rt_pin_mode(LED0_PIN, PIN_MODE_OUTPUT);
    InitFileSys();
    ftp_init(2048, 27, 100);
    while (count++)
    {
        rt_pin_write(LED0_PIN, PIN_HIGH);
        rt_thread_mdelay(500);
        rt_pin_write(LED0_PIN, PIN_LOW);
        rt_thread_mdelay(500);
    }

    return RT_EOK;
}
